from rich import print


def find_missing_registrant(registrants: list[str], finishers: list[str]):
    racers = {}
    for registrant in registrants:
        racers[registrant] = False
    for finisher in finishers:
        racers[finisher] = True
    for racer in racers:
        if not racers[racer]:
            return racer


print(find_missing_registrant(["leo", "karl", "eden"], ["eden", "karl"]))
print(
    find_missing_registrant(
        [
            "duska",
            "noor",
            "kala",
            "idris",
            "haris",
            "ken",
            "zahara",
            "raisa",
            "anima",
            "basia",
            "evander",
            "malik",
            "asa",
            "lina",
            "caris",
            "baz",
            "nia",
            "asha",
            "kamal",
            "paz",
            "azami",
            "lulu",
            "kim",
            "samir",
            "laila",
            "talia",
        ],
        [
            "raisa",
            "idris",
            "lulu",
            "haris",
            "duska",
            "lina",
            "azami",
            "caris",
            "kim",
            "evander",
            "noor",
            "paz",
            "asha",
            "zahara",
            "anima",
            "laila",
            "samir",
            "ken",
            "asa",
            "talia",
            "basia",
            "kala",
            "kamal",
            "baz",
            "nia",
        ],
    )
)
print(
    find_missing_registrant(
        [
            "caris",
            "malik",
            "duska",
            "lulu",
            "asha",
            "idris",
            "kim",
            "anima",
            "basia",
            "kamal",
            "lina",
            "laila",
            "asa",
            "nia",
            "baz",
            "evander",
            "azami",
            "haris",
            "kala",
            "ken",
        ],
        [
            "idris",
            "anima",
            "kala",
            "haris",
            "nia",
            "evander",
            "kamal",
            "duska",
            "lulu",
            "laila",
            "azami",
            "ken",
            "caris",
            "kim",
            "asa",
            "asha",
            "malik",
            "baz",
            "lina",
        ],
    )
)
