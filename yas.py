def yas(num_a):
    return f"Y{'a' * num_a}!"


print(yas(1))
print(yas(3))
print(yas(5))
print(yas(7))
