from rich import print


def profit(
    num_cupcakes: int, num_tubes_frosting: int, tubes_per_cupcake: int
) -> int | None:
    if tubes_per_cupcake == 0:
        raise ValueError("tubes per cupcake cannot be 0")
    num_frosted = num_tubes_frosting // tubes_per_cupcake
    leftover_tubes = num_tubes_frosting % tubes_per_cupcake
    num_unfrosted = num_cupcakes - num_frosted
    return num_frosted * 10 + num_unfrosted * 4 + leftover_tubes


print(profit(10, 5, 2))
